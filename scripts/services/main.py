from scripts.core.handlers.create_operation import create
from scripts.core.handlers.update_operation import update
from scripts.core.handlers.read_operation import read
from scripts.core.handlers.delete_operation import delete


def main():
    while 1:

        # Select your option
        selection = input(
            '\nSelect 1 to insert data, 2 to read data, 3 to update data, 4 to delete data, 5 to Stop CRUD'
            ' operations >> \n')
        if selection == '1':
            create()

        elif selection == '2':
            read()

        elif selection == '3':
            update()

        elif selection == '4':
            delete()

        elif selection == '5':
            print("Successfully stopped CRUD Operations!")
            quit()

        else:
            print('\n INVALID SELECTION!\n -- Try Again! --\n')
            main()
