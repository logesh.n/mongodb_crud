from scripts.core.engine.database import collection


def read():
    try:
        read_data = collection.find()
        print('\n All data from Movie Database \n')
        for read in read_data:
            print("Movie details are >> ", read)

    except Exception as e:
        print("Error details >> ", e)
