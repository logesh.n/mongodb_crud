from scripts.core.engine.database import collection


# Function to insert the data into mongodb

def update():

    try:
        movie_id = int(input("enter movie id >> "))
        movie_name = input("enter movie name >> ")
        movie_year = int(input("enter movie released year/expected release year >> "))
        movie_genre = input("enter movie genre >> ")
        movie_type = input("enter the origin of release (Kollywood..like that) >> ")

        collection.update_one(
            {"movie_id": movie_id},
            {
               "$set": {
                    "movie_id": movie_id,
                    "movie_name": movie_name,
                    "movie_year": movie_year,
                    "movie_genre": movie_genre,
                    "movie_type": movie_type
                }
            }
        )
        print("\nRecords updated successfully!")

    except Exception as e:
        print("Error details >> ", e)

