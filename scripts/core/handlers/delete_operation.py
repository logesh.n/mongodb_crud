from scripts.core.engine.database import collection


# Function to insert the data into mongodb

def delete():
    try:
        movie_id = int(input("enter movie id to delete >> "))

        collection.delete_many({"movie_id": movie_id})
        print("\nDeleted data from the database!")

    except Exception as e:
        print("Error details >> ", e)
